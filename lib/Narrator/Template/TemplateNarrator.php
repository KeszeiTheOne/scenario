<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Narrator\Template;

use Keszei\Scenario\Narrator;
use Keszei\Scenario\ParameterResolver;

class TemplateNarrator implements Narrator {

	private $name;

	private $parameters;

	private $statusCode;

	public function __construct($name, $parameters, $statusCode = 200) {
		$this->name = $name;
		$this->parameters = $parameters;
		$this->statusCode = $statusCode;
	}

	public function tellStory($stimulus, $story) {
		return new Template($this->name, $this->resolveParameters($stimulus, $story), $this->statusCode);
	}

	private function resolveParameters($stimulus, $story) {
		if ($this->parameters instanceof ParameterResolver) {
			return $this->parameters->tellStory($stimulus, $story);
		}

		return $this->parameters;
	}

}
