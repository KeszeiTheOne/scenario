<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Narrator\ClientError;

class ClientError {

	private $statusCode;

	private $message;

	public function __construct($statusCode, $message) {
		$this->statusCode = $statusCode;
		$this->message = $message;
	}

	public function getStatusCode() {
		return $this->statusCode;
	}

	public function getMessage() {
		return $this->message;
	}

}
