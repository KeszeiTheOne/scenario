<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Action;

use Keszei\Scenario\ParameterResolver;

class CallbackParameterResolver implements ParameterResolver {

	private $callback;

	public function __construct($callback) {
		$this->callback = $callback;
	}

	public function tellStory($stimulus, $story) {
		return call_user_func($this->callback, $stimulus, $story);
	}

}
