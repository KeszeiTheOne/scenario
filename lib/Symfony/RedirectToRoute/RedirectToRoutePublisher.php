<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Symfony\RedirectToRoute;

use Keszei\Scenario\Exception\UnexpectedNarrative;
use Keszei\Scenario\Narrator\RedirectToRoute\RedirectToRoute;
use Keszei\Scenario\Publisher;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

class RedirectToRoutePublisher implements Publisher {

	/**
	 * @var Router
	 */
	private $router;

	public function __construct(Router $router) {
		$this->router = $router;
	}

	public function publishNarrative($narrative) {
		if (!$narrative instanceof RedirectToRoute) {
			throw new UnexpectedNarrative;
		}

		return new RedirectResponse($this->router->generate($narrative->getRoute(), $narrative->getParameters()));
	}

}
