<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Symfony\ClientError;

use Keszei\Scenario\Exception\UnexpectedNarrative;
use Keszei\Scenario\Narrator\ClientError\ClientError;
use Keszei\Scenario\Publisher;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ClientErrorPublisher implements Publisher {

	public function publishNarrative($narrative) {
		if (!$narrative instanceof ClientError) {
			throw new UnexpectedNarrative;
		}

		throw new HttpException($narrative->getStatusCode(), $narrative->getMessage());
	}

}
