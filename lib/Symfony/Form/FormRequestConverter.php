<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Symfony\Form;

use Keszei\Scenario\RequestConverter;
use Keszei\Scenario\Symfony\Form\Exception\InvalidForm;
use Keszei\Scenario\Symfony\Form\Exception\UnsubmittedForm;
use Symfony\Component\Form\FormFactory;

class FormRequestConverter implements RequestConverter {

	/**
	 * @var FormFactory
	 */
	private $formFactory;

	private $type;

	private $options = [];

	public function __construct(FormFactory $formFactory) {
		$this->formFactory = $formFactory;
	}

	public function createRequest($stimulus) {
		$form = $this->formFactory->create($this->type, null, $this->resolveOptions($stimulus));
		$form->handleRequest($stimulus);

		if (!$form->isValid()) {
			throw new InvalidForm($form);
		}

		if (!$form->isSubmitted()) {
			throw new UnsubmittedForm($form);
		}

		return $form->getData();
	}

	private function resolveOptions($stimulus) {
		if (is_callable($this->options)) {
			return call_user_func($this->options, $stimulus);
		}

		return $this->options;
	}

	public function addOption($name, $value) {
		$this->options[$name] = $value;
		return $this;
	}

	public function setOptions($options) {
		$this->options = $options;
		return $this;
	}

	public function setType($type) {
		$this->type = $type;
		return $this;
	}

}
