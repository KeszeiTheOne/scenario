<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Symfony\Template;

use Keszei\Scenario\Exception\UnexpectedNarrative;
use Keszei\Scenario\Narrator\Template\Template;
use Keszei\Scenario\Publisher;
use Symfony\Component\HttpFoundation\Response;
use Twig_Environment;

class TemplatePublisher implements Publisher {

	/**
	 * @var Twig_Environment
	 */
	private $twig;

	public function __construct(Twig_Environment $twig) {
		$this->twig = $twig;
	}

	public function publishNarrative($narrative) {
		if (!$narrative instanceof Template) {
			throw new UnexpectedNarrative;
		}

		return new Response($this->renderTemplate($narrative), $narrative->getStatusCode());
	}
	
	private function renderTemplate(Template $narrative) {
		return $this->twig->render($narrative->getName(), $narrative->getParameters());
	}

}
