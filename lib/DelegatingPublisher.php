<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario;

use Keszei\Scenario\Exception\UnexpectedNarrative;

class DelegatingPublisher implements Publisher {

	/**
	 * @var Publisher[]
	 */
	private $publishers = [];

	public function publishNarrative($narrative) {
		foreach ($this->publishers as $publisher) {
			try {
				return $publisher->publishNarrative($narrative);
			}
			catch (UnexpectedNarrative $exc) {
				
			}
		}

		throw new UnexpectedNarrative;
	}

	public function setPublishers($publishers) {
		foreach ($publishers as $publisher) {
			$this->addPublisher($publisher);
		}
		return $this;
	}

	public function addPublisher(Publisher $publisher) {
		$this->publishers[] = $publisher;

		return $this;
	}

}
